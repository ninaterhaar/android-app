package com.example.pokemontrainermanagementsystem.collection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.pokemontrainermanagementsystem.R;

public class PokemonView extends ConstraintLayout {

    private ImageView pokemonImage;
    private TextView pokemonName;

    public PokemonView(Context context) {
        super(context);
        init();
    }

    public PokemonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PokemonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.pokemon_spinner_row, this);

        pokemonImage = findViewById(R.id.image_pokemon);
        pokemonName = findViewById(R.id.name_pokemon);

    }
}

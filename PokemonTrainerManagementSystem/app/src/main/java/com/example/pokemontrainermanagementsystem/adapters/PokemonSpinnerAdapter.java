package com.example.pokemontrainermanagementsystem.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.item.PokemonItem;

import java.util.ArrayList;

public class PokemonSpinnerAdapter extends ArrayAdapter<PokemonItem> {

    public PokemonSpinnerAdapter(Context context, ArrayList<PokemonItem> pokemonList){
        super(context,0, pokemonList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView,  parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView,  parent);
    }

    private View initView(int position, View convertView,  ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.pokemon_spinner_row, parent, false);
        }
        ImageView imageViewFlag = convertView.findViewById(R.id.image_pokemon);
        TextView textViewName = convertView.findViewById(R.id.name_pokemon);

        PokemonItem currentPokemon = getItem(position);

        if (currentPokemon !=  null){
        imageViewFlag.setImageResource(currentPokemon.getmPokemonImage());
        textViewName.setText(currentPokemon.getmPokemonName());}

        return convertView;
    }
}

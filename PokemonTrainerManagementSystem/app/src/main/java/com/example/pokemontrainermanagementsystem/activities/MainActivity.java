package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.pokemontrainermanagementsystem.R;

public class MainActivity extends AppCompatActivity {

    public Button see_trainers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buttonSeeTrainers();

    }
    public void buttonSeeTrainers(){
        see_trainers = (Button) findViewById(R.id.button_see_trainers);
        see_trainers.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                openSeeTrainerActivity();
            }
        });
    }

    public void openSeeTrainerActivity(){
        Intent intent = new Intent(this, TrainerOverviewActivity.class);
        startActivity(intent);
    }
}



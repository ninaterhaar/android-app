package com.example.pokemontrainermanagementsystem.dataprovider;

import com.example.pokemontrainermanagementsystem.pokemon.Pokemon;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

import java.util.ArrayList;

public class DataProvider {

    private ArrayList<Trainer> trainers;
    private static DataProvider instance;


    private DataProvider(){
        initialize();
    }

    public void initialize(){
        trainers = new ArrayList<>();

        if (trainers.size() == 0){
            Trainer niels = new Trainer("Niels");
            Trainer nick = new Trainer("Nick");
            trainers.add(niels); // test trainer 1
            trainers.add(nick); // test trainer 2
        }
        else{

        }
    }

    public static DataProvider getInstance(){
        if(instance == null){
            instance = new DataProvider();
        }
        return instance;
    }

    public ArrayList<Trainer> getTrainersList() {
        return trainers;
    }

}

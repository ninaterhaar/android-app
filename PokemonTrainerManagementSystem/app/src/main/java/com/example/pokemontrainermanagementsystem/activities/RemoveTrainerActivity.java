package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

import static com.example.pokemontrainermanagementsystem.activities.TrainerOverviewActivity.trainers;

public class RemoveTrainerActivity extends AppCompatActivity {


    ListView listView;
    public ArrayAdapter<Trainer> TrainerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_trainer);


        TrainerAdapter = new ArrayAdapter<Trainer>(this, android.R.layout.simple_list_item_1, trainers);
        listView = (ListView) findViewById(R.id.rm_trainer_lv);
        listView.setAdapter(TrainerAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast toast;
                Context context = getApplicationContext();
                CharSequence text = "Trainer: " + trainers.get(position).getName() + " has been removed.";
                int duration = Toast.LENGTH_SHORT;
                toast = Toast.makeText(context, text, duration);
                toast.show();

                trainers.remove(position);
                finish();
            }

        });


    }
}

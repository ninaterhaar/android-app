package com.example.pokemontrainermanagementsystem.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

import java.util.ArrayList;

public class TrainerAdapter extends ArrayAdapter<Trainer> {

    ArrayList<Trainer> mTrainer;
    LayoutInflater mInflater;

    public TrainerAdapter(Context context, ArrayList<Trainer> trainerObjects) {
        super(context, R.layout.trainer_adapter_view, trainerObjects);

        mTrainer = trainerObjects;
        mInflater = LayoutInflater.from(context);
    }



}

package com.example.pokemontrainermanagementsystem.customView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.annotation.Nullable;

import com.example.pokemontrainermanagementsystem.activities.PokemonDetailActivity;

public class IVDrawable extends View {

    private int width;
    private int height;
    private Paint paint;
    private Rect rectangle;
    private DisplayMetrics metrics;
    int dpi;



    public IVDrawable(Context context) {
        super(context);
        init();
    }

    public IVDrawable(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IVDrawable(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public IVDrawable(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init(){

        metrics = getResources().getDisplayMetrics();
        dpi = metrics.densityDpi;
        width = (370 * dpi)/160;
        height = (25 * dpi)/160;
        paint = new Paint();

    }

    public void setColorGreen(){
        paint.setColor(Color.GREEN);
        rectangle = new Rect(0,0, (int) width * 2,height);
        invalidate();
    }

    public void setColorYellow(){
        paint.setColor(Color.YELLOW);
        rectangle = new Rect(0,0, 185, height);
        invalidate();
    }

    public void setColorRed(){
        paint.setColor(Color.RED);
        rectangle = new Rect(0,0, 95, height);
        invalidate();

    }
    public void setColorGray(){
        paint.setColor(Color.LTGRAY);
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(rectangle,paint);


    }
}

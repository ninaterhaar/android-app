package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;
import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class TrainerOverviewActivity extends AppCompatActivity {

    public static final String TRAINER_POS_KEY = "trainerposition";
    ListView listView;
    private Button button_add_trainer;                          // button to remove trainers.
    private Button button_remove_trainer;                       // button to remove trainers.
    protected static ArrayList<Trainer> trainers; // = new ArrayList<>();   // collection of trainers.
    public ArrayAdapter<Trainer> TrainerAdapter;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trainer_list_overview);
        trainers = DataProvider.getInstance().getTrainersList();
        initialize();

        TrainerAdapter = new ArrayAdapter<Trainer>(this, android.R.layout.simple_list_item_1, trainers);
        listView = (ListView) findViewById(R.id.trainer_listview);
        listView.setAdapter(TrainerAdapter);

        buttonAddTrainerActivity();     // if button is clicked start activity.
        buttonRemoveTrainerActivity();  // if button is clicked start activity.

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), PokemonOverviewActivity.class);
                intent.putExtra(TRAINER_POS_KEY, position);
                startActivity(intent);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        TrainerAdapter.notifyDataSetChanged();
        trainers = DataProvider.getInstance().getTrainersList();
    }

    public void initialize(){


    }

    // button to start add activity.
    public void buttonAddTrainerActivity(){
        button_add_trainer = (Button) findViewById(R.id.btn_add_trainer);
        button_add_trainer.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                openAddTrainerActivity();
            }
        });
    }

    public void openAddTrainerActivity(){
        Intent intent = new Intent(this, AddTrainerActivity.class);
        startActivity(intent);
    }

    // button to start remove activity.
    public void buttonRemoveTrainerActivity(){
        button_remove_trainer = (Button) findViewById(R.id.btn_remove_trainer);
        button_remove_trainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRemoveTrainerActivity();
            }
        });
    }
    public void openRemoveTrainerActivity(){
        Intent intent = new Intent(this, RemoveTrainerActivity.class);
        startActivity(intent);
    }

    public static List<Trainer> getTrainers() {
        return trainers;
    }
}

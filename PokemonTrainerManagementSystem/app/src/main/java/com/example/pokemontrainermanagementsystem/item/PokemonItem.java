package com.example.pokemontrainermanagementsystem.item;

import android.os.Parcel;
import android.os.Parcelable;

public class PokemonItem {

    private String mPokemonName;
    private int mPokemonImage;


    public PokemonItem(String pokemonName, int pokemonImage) {
        this.mPokemonImage = pokemonImage;
        this.mPokemonName = pokemonName;

    }


    public int getmPokemonImage() {
        return mPokemonImage;
    }

    public String getmPokemonName() {
        return mPokemonName;
    }
}



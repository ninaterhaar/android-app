package com.example.pokemontrainermanagementsystem.trainer;

import android.provider.ContactsContract;

import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.pokemon.Pokemon;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Trainer {

    private String name; //name of the trainer.
    private ArrayList<Pokemon> pokemons;


    /**
     *
     * @param name name of the created trainer.
     * Each trainer has a own list of Pokemon that he/she owns.
     */

    public Trainer(String name){
        this.name = name;
        pokemons = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Pokemon> getPokemonList() {
        return pokemons;
    }

    public String toString(){
        return "Trainer Name: " + getName();
    }
}

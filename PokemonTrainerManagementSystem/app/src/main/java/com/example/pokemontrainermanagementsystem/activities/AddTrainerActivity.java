package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;



public class AddTrainerActivity extends AppCompatActivity {

    Button button_save;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trainer);





        /**
         * @param button_save is connected to the button on the activity.
         */

        button_save = (Button) findViewById(R.id.save_trainer); // save the trainer.
        button_save.setOnClickListener(new View.OnClickListener(){

            CharSequence text;
            int duration;
            Context context;

            public void onClick(View v){
                EditText nameTrainer = (EditText) findViewById(R.id.choose_trainer_name);
                String name = nameTrainer.getText().toString();
                Trainer trainer = new Trainer(name);

                Toast toast;
                context = getApplicationContext();
                text = "Trainer: " + name + " has been added.";
                duration = Toast.LENGTH_SHORT;
                toast = Toast.makeText(context, text, duration);
                toast.show();

                DataProvider.getInstance().getTrainersList().add(trainer);
                finish(); // finishes the button.
            }
        });


    }

}

package com.example.pokemontrainermanagementsystem.collection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.pokemontrainermanagementsystem.R;

public class PokemonStats extends ConstraintLayout {

    private EditText hitPoints;
    private EditText combatPower;
    private EditText defense;
    private EditText speed;


    public PokemonStats(Context context) {
        super(context);
        init();
    }

    public PokemonStats(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PokemonStats(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.pokemon_stats, this);

        hitPoints = findViewById(R.id.et_hitpoints);
        combatPower = findViewById(R.id.et_combatPower);
        defense = findViewById(R.id.et_defense);
        speed = findViewById(R.id.et_speed);
    }
}

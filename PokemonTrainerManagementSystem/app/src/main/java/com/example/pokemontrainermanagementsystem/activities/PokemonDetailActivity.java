package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.customView.IVDrawable;

public class PokemonDetailActivity extends AppCompatActivity {

    private ImageView pokemonImage;
    private TextView tv_NickName;
    private TextView tvNickName;
    private TextView tvPokemonName;
    private TextView tvHitPoints;
    private TextView tvCombatPower;
    private TextView tvDefense;
    private TextView tvSpeed;
    private IVDrawable ivDrawable;
    private double calculateIV;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detail);

        ivDrawable = (IVDrawable) findViewById(R.id.ivView);


        Intent intent = getIntent();
        String pokemonName = intent.getStringExtra(PokemonOverviewActivity.POKEMON_NAME_KEY);
        String pokemonNickName = intent.getStringExtra(PokemonOverviewActivity.NICKNAME_KEY);
        int combatPower = intent.getIntExtra(PokemonOverviewActivity.COMBATPOWER_KEY,0);
        int hitPoints = intent.getIntExtra(PokemonOverviewActivity.HITPOINTS_KEY, 0);
        int defense = intent.getIntExtra(PokemonOverviewActivity.DEFENSE_KEY, 0);
        int speed = intent.getIntExtra(PokemonOverviewActivity.SPEED_KEY, 0);
        int image = intent.getIntExtra(PokemonOverviewActivity.POKEMON_IMAGE,0);

        tvPokemonName = findViewById(R.id.tvPokemonName);
        tvPokemonName.setText(pokemonName);

        tvNickName = findViewById(R.id.tvNickname);
        assert pokemonNickName != null;
        if (pokemonNickName.equals("")){
            tvNickName.setText("This Pokemon has no nickname");
        }
        else{
            tvNickName.setText(pokemonNickName);
        }

        tvCombatPower = findViewById(R.id.tvCombatPower);
        tvCombatPower.setText(Integer.toString(combatPower));

        tvHitPoints = findViewById(R.id.tvHitpointsDetail);
        tvHitPoints.setText(Integer.toString(hitPoints));

        tvDefense = findViewById(R.id.tvDefense);
        tvDefense.setText(Integer.toString(defense));

        tvSpeed = findViewById(R.id.tvSpeed);
        tvSpeed.setText(Integer.toString(speed));

        pokemonImage = findViewById(R.id.detailPokemonImage);
        pokemonImage.setImageResource(image);

        /**
         * The calculation for the IV is working in the following way.
         * Each stats will be divided by '2' before it will be counted with another stat.
         * (combatPower / 2 + hitPoints / 2 + defense / 2 + speed / 2).
         * if the total amount is has been calculated it will go times 2 and then divided by 10.
         */
        calculateIV = 2 * (combatPower / 3 + hitPoints / 3 + defense / 3 + speed / 3) / 10;

        if (calculateIV > 100){
            ivDrawable.setColorGreen();
            Toast.makeText(PokemonDetailActivity.this,"The pokemon has a IV of 100%", Toast.LENGTH_SHORT).show();
        }
        else if (calculateIV <= 100 && calculateIV >= 80){
            ivDrawable.setColorGreen();
            Toast.makeText(PokemonDetailActivity.this,"The pokemon has a IV of " + calculateIV + "%", Toast.LENGTH_SHORT).show();
        }
        else if(calculateIV >= 25 && calculateIV < 80){
            ivDrawable.setColorYellow();
            Toast.makeText(PokemonDetailActivity.this,"The pokemon has a IV of " + calculateIV + "%", Toast.LENGTH_SHORT).show();
        }
        else if(calculateIV >= 1 && calculateIV < 25){
            ivDrawable.setColorRed();
            Toast.makeText(PokemonDetailActivity.this,"The pokemon has a IV of " + calculateIV + "%", Toast.LENGTH_SHORT).show();
        }
        else{
            ivDrawable.setColorGray();
            Toast.makeText(PokemonDetailActivity.this,"The pokemon has no IV value", Toast.LENGTH_SHORT).show();
        }

    }
    public void setColorIvBar(){




    }
}

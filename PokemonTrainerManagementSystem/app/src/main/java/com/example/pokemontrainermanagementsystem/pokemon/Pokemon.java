package com.example.pokemontrainermanagementsystem.pokemon;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.widget.ImageView;

import androidx.annotation.NonNull;

public class Pokemon {


    private String name; // name of the pokemon.
    private String nickName; // a nickname you can give to a pokemon.
    private int hitPoints; // amount of hit points.
    private int combatPower; // amount of combatPower of a pokemon.
    private int defense; // amount of defense of a pokemon.
    private int speed; // amount of speed of a pokemon.
    private int pokemonImage;


    /**
     *
     * @param name name of the pokemon.
     */

    public Pokemon(String name){
        this.name = name;
    }


    /**
     * All getters provided
     *
     *
     */

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getCombatPower() {
        return combatPower;
    }

    public int getDefense() {
        return defense;
    }

    public int getSpeed() {
        return speed;
    }

    public String getNickName() {
        return nickName;
    }

    public int getPokemonImage() {
        return pokemonImage;
    }

    /**
     * All setters provided for the Add Pokemon Activity.
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setCombatPower(int combatPower) {
        this.combatPower = combatPower;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setPokemonImage(int pokemonImage) {
        this.pokemonImage = pokemonImage;
    }

    /**
     *
     * @return The name of the pokemon and the amount hitpoints for the listview.
     */
    @Override
    public String toString() {
        return "Pokemon: " + getName();
        //+ " | Hitpoints: " + getHitPoints() + " | CP: " + getCombatPower();
    }
}

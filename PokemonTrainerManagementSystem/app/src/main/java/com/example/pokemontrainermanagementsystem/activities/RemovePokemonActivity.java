package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.pokemon.Pokemon;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

import java.util.ArrayList;
import java.util.List;

import static com.example.pokemontrainermanagementsystem.activities.TrainerOverviewActivity.trainers;

public class RemovePokemonActivity extends AppCompatActivity {


    private ListView listViewPokemon;
    public ArrayAdapter<Pokemon> PokemonAdapter;
    private ArrayList<Pokemon> pokemonList;
    private int trainerposition;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_pokemon);

        Intent intent = getIntent();
        trainerposition = intent.getIntExtra(TrainerOverviewActivity.TRAINER_POS_KEY, -1);

        ArrayList<Trainer> listTrainers = trainers;
        Trainer trainer = listTrainers.get(trainerposition);
        pokemonList = trainer.getPokemonList();

        PokemonAdapter = new ArrayAdapter<Pokemon>(this, android.R.layout.simple_list_item_1, pokemonList);
        listViewPokemon = (ListView) findViewById(R.id.lv_pokemon);
        listViewPokemon.setAdapter(PokemonAdapter);

        listViewPokemon.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast toast;
                Context context = getApplicationContext();
                CharSequence text = pokemonList.get(position).getName() +  " has been released in the wild!";
                int duration = Toast.LENGTH_SHORT;
                toast = Toast.makeText(context, text, duration);
                toast.show();
                DataProvider.getInstance().getTrainersList().get(trainerposition).getPokemonList().remove(position);
                finish();
            }

        });



    }
}

package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;

public class EditTrainerActivity extends AppCompatActivity {

    private int trainerposition;
    private String oldName;
    //private String newName;
    private TextView tvOldTrainerName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_trainer);


        Intent intent = getIntent();
        trainerposition = intent.getIntExtra(TrainerOverviewActivity.TRAINER_POS_KEY, -1);

        final EditText editedTrainerName = findViewById(R.id.etEditNameTrainer);
        final String newName = editedTrainerName.getText().toString();
        final Trainer trainer = DataProvider.getInstance().getTrainersList().get(trainerposition);

        tvOldTrainerName = findViewById(R.id.tvOldTrainerName);
        tvOldTrainerName.setText("The current name is: " +  DataProvider.getInstance().getTrainersList().get(trainerposition).getName());

        Button btnSaveEdits = findViewById(R.id.btnSaveEditedTrainer);
        btnSaveEdits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldName = DataProvider.getInstance().getTrainersList().get(trainerposition).getName();
                Toast.makeText(EditTrainerActivity.this,"Your name has changed to: " + editedTrainerName.getText().toString() + ", your previous name was: " + oldName, Toast.LENGTH_SHORT).show();
                trainer.setName(editedTrainerName.getText().toString());
                finish();
            }
        });


    }
}

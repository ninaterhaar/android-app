package com.example.pokemontrainermanagementsystem.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.pokemon.Pokemon;
import com.example.pokemontrainermanagementsystem.trainer.Trainer;
import static com.example.pokemontrainermanagementsystem.activities.TrainerOverviewActivity.trainers;

import java.util.ArrayList;
import java.util.List;

public class PokemonOverviewActivity extends AppCompatActivity {

    /**
     *  These attributes are needed to send specific data to the Pokemon Detail Activity.
     */

    public static final String POKEMON_IMAGE = "pokemon_image";
    public static final String POKEMON_NAME_KEY = "pokemon_name_key";
    public static final String NICKNAME_KEY = "nickname_key";
    public static final String HITPOINTS_KEY = "hitpoints_key";
    public static final String COMBATPOWER_KEY = "combatpower_key";
    public static final String DEFENSE_KEY = "defense_key";
    public static final String SPEED_KEY = "speed_key";


    /**
     *  TODO: JavaDoc this.
     */
    public ArrayAdapter<Pokemon> PokemonAdapter;
    private ListView pokemonListView;
    private Button addPokemon;
    private Button releasePokemon;
    private Button editTrainer;
    private TextView totalCatchedPokemon;
    private TextView trainerName;
    private Trainer trainer;


    private ArrayList<Pokemon> pokemonList;
    private int trainerposition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list_overview);

        Intent intent = getIntent();
        trainerposition = intent.getIntExtra(TrainerOverviewActivity.TRAINER_POS_KEY,-1);

        ArrayList<Trainer> listTrainers = trainers;
        trainer = listTrainers.get(trainerposition);
        pokemonList = trainer.getPokemonList();


        totalCatchedPokemon = findViewById(R.id.tvTotalPokemonCatched);
        totalCatchedPokemon.setText(Integer.toString(pokemonList.size()));

        trainerName = findViewById(R.id.trainerName);
        trainerName.setText(trainer.getName());


        PokemonAdapter = new ArrayAdapter<Pokemon>(this, android.R.layout.simple_list_item_1, pokemonList);
        pokemonListView = (ListView) findViewById(R.id.lv_pokemon);
        pokemonListView.setAdapter(PokemonAdapter);


        buttonAddPokemonActivity();
        releasePokemon();
        btnEditTrainerActivity();
        PokemonAdapter.notifyDataSetChanged();

        pokemonListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pokemon pokemon = PokemonAdapter.getItem(position);
                Intent intent = new Intent(getApplicationContext(), PokemonDetailActivity.class);
                intent.putExtra(POKEMON_NAME_KEY, pokemon.getName());
                intent.putExtra(NICKNAME_KEY, pokemon.getNickName());
                intent.putExtra(HITPOINTS_KEY, pokemon.getHitPoints());
                intent.putExtra(COMBATPOWER_KEY, pokemon.getCombatPower());
                intent.putExtra(DEFENSE_KEY, pokemon.getDefense());
                intent.putExtra(SPEED_KEY, pokemon.getSpeed());
                intent.putExtra(POKEMON_IMAGE, pokemon.getPokemonImage());
                startActivity(intent);
            }
        });


    }
    protected void onResume() {
        super.onResume();
        totalCatchedPokemon.setText(Integer.toString(pokemonList.size()));
        trainerName.setText(trainer.getName());
        PokemonAdapter.notifyDataSetChanged();
    }

    // button to start add activity.
    public void buttonAddPokemonActivity(){
        addPokemon = (Button) findViewById(R.id.btnAddPokemon);
        addPokemon.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                openAddPokemonActivity();
            }
        });
    }

    public void openAddPokemonActivity(){
        Intent intent = new Intent(this, AddPokemonActivity.class);
        intent.putExtra(TrainerOverviewActivity.TRAINER_POS_KEY, trainerposition);
        startActivity(intent);
    }

    public void releasePokemon(){
        releasePokemon = (Button) findViewById(R.id.btnReleasePokemon);
        releasePokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openReleasePokemonActivity();
            }
        });
    }

    public void openReleasePokemonActivity(){
        Intent intent = new Intent(this, RemovePokemonActivity.class);
        intent.putExtra(TrainerOverviewActivity.TRAINER_POS_KEY, trainerposition);
        startActivity(intent);
    }


    public void btnEditTrainerActivity(){
        editTrainer = (Button) findViewById(R.id.btnEditTrainer);
        editTrainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditTrainerActivity();
            }
        });
    }
    public void openEditTrainerActivity(){
        Intent intent = new Intent(this, EditTrainerActivity.class);
        intent.putExtra(TrainerOverviewActivity.TRAINER_POS_KEY, trainerposition);
        startActivity(intent);
    }


}

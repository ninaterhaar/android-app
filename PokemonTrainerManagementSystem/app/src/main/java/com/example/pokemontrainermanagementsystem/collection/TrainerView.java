package com.example.pokemontrainermanagementsystem.collection;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.pokemontrainermanagementsystem.R;

public class TrainerView extends ConstraintLayout {

    private ImageView trainerPicture;
    private TextView trainerTvName;
    private TextView totalAmountOfPokemon;

    public TrainerView(Context context) {
        super(context);
        init();
    }

    public TrainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TrainerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.trainer_view, this);

        trainerPicture = findViewById(R.id.imageTrainer);
        trainerTvName = findViewById(R.id.trainerName);
        totalAmountOfPokemon = findViewById(R.id.tvTotalPokemonCatched);
    }
}

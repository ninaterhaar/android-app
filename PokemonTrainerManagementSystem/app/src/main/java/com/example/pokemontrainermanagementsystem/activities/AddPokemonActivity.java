package com.example.pokemontrainermanagementsystem.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pokemontrainermanagementsystem.R;
import com.example.pokemontrainermanagementsystem.dataprovider.DataProvider;
import com.example.pokemontrainermanagementsystem.item.PokemonItem;
import com.example.pokemontrainermanagementsystem.adapters.PokemonSpinnerAdapter;
import com.example.pokemontrainermanagementsystem.pokemon.Pokemon;


import java.util.ArrayList;


public class AddPokemonActivity extends AppCompatActivity {

    private ArrayList<PokemonItem> mPokemonSpinnerList;
    private PokemonSpinnerAdapter pokemonSpinnerAdapter;
    private Button savePokemon;

    private Switch nickname;
    private EditText chosenNickname;
    private Spinner spinnerPokemon;
    private int trainerposition;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pokemon);

        Intent intent = getIntent();
        trainerposition = intent.getIntExtra(TrainerOverviewActivity.TRAINER_POS_KEY, -1);

        initList();
        savePokemon();


        spinnerPokemon = findViewById(R.id.available_pokemon);
        pokemonSpinnerAdapter = new PokemonSpinnerAdapter(this, mPokemonSpinnerList);
        spinnerPokemon.setAdapter(pokemonSpinnerAdapter);

        chosenNickname = findViewById(R.id.nickname_et);
        nickname = findViewById(R.id.nickname_sw);



        chosenNickname.setVisibility(View.INVISIBLE);
                nickname.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chosenNickname.setVisibility(View.VISIBLE);
                }
                else{
                    chosenNickname.setVisibility(View.INVISIBLE);
                }
            }
        });


        spinnerPokemon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PokemonItem clickedPokemonItem = (PokemonItem) parent.getItemAtPosition(position);
                String clickedPokemonName = clickedPokemonItem.getmPokemonName();
                Toast.makeText(AddPokemonActivity.this,clickedPokemonName + " selected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void initList(){

        mPokemonSpinnerList = new ArrayList<>();
        mPokemonSpinnerList.add(new PokemonItem("Bulbasaur", R.drawable.bulbasaur));
        mPokemonSpinnerList.add(new PokemonItem("Ivysaur", R.drawable.ivysaur));
        mPokemonSpinnerList.add(new PokemonItem("Venusaur", R.drawable.venusaur));
        mPokemonSpinnerList.add(new PokemonItem("Charmander",R.drawable.charmander));
        mPokemonSpinnerList.add(new PokemonItem("Charmeleon",R.drawable.charmeleon));
        mPokemonSpinnerList.add(new PokemonItem("Charizard",R.drawable.charizard));
    }
    public void addDetailsToPokemon(){

        EditText filledInHitpoints = findViewById(R.id.et_hitpoints);
        EditText filledInCombatPower = findViewById(R.id.et_combatPower);
        EditText filledInDefense = findViewById(R.id.et_defense);
        EditText filledInSpeed = findViewById(R.id.et_speed);
        EditText filledInNickname = findViewById(R.id.nickname_et);

        int hitPoints = Integer.parseInt(filledInHitpoints.getText().toString());
        int combatPower = Integer.parseInt(filledInCombatPower.getText().toString());
        int defense = Integer.parseInt(filledInDefense.getText().toString());
        int speed = Integer.parseInt(filledInSpeed.getText().toString());
        String nickname = filledInNickname.getText().toString();


        for (int i = 0; i < mPokemonSpinnerList.size(); i++){
            if(spinnerPokemon.getSelectedItemPosition() == i){
                Pokemon pokemon = new Pokemon(mPokemonSpinnerList.get(i).getmPokemonName());
                pokemon.setHitPoints(hitPoints);
                pokemon.setCombatPower(combatPower);
                pokemon.setDefense(defense);
                pokemon.setSpeed(speed);
                pokemon.setNickName(nickname);
                pokemon.setPokemonImage(mPokemonSpinnerList.get(i).getmPokemonImage());
                DataProvider.getInstance().getTrainersList().get(trainerposition).getPokemonList().add(pokemon);
                break;
            }
        }
    }

    public void savePokemon(){
        savePokemon = (Button) findViewById(R.id.btnSavePokemon);
        savePokemon.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                addDetailsToPokemon();
                finish();
            }
        });
    }
}
